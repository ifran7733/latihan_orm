const express = require('express');
const app = express();
require('dotenv').config();
const port = process.env.PORT || 3500;
const cors = require('cors');
const bodyParser = require('body-parser');
const router = require('./router');

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use('/api', router);

app.listen(port, () => {
  console.log(`server running at port ${port}`);
});
